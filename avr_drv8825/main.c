#define F_CPU 8000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <inttypes.h>

#define BAUD 9600
#define MYUBRR F_CPU/16/BAUD-1

#define TRUE 1
#define FALSE 0

#define DIRECTION PB0
#define STEP PB1
#define SLEEP PD2
#define RESET PD3
#define MS3 PD4
#define MS2 PD5
#define MS1 PD6
#define MOT_EN PD7

volatile unsigned char UART_data_in[8];
volatile unsigned short UART_data_count = 0;
volatile unsigned char command_ready = FALSE;


void UART_Init(uint16_t ubrr) {
    UBRRH = (uint8_t)(ubrr >> 8);
    UBRRL = (uint8_t)ubrr;
    UCSRB = (1 << RXEN) | (1 << TXEN)|(1 << RXCIE);
    UCSRC = (1 << UCSZ0) | (1 << UCSZ1) | (1 << URSEL);
}

void UART_CharSend(unsigned char UART_Data) {
    while (!(UCSRA & (1 << UDRE)));
    UDR = UART_Data;
}

void UART_Send(const char *s) {
    while (*s) {
	UART_CharSend(*s++);
    }
}

unsigned char UART_CharRead(void) {
    while (!(UCSRA & (1 << RXC))); // Waiting for data to be received
    return UDR; // Get and return received data from buffer
}

//unsigned char *UART_Read(void) {
//    while(1) {
//	
//    }
//}

int cnt = 1;

int main(void) {
  
    // Porty jako wyjscia
    DDRD |= (1<<SLEEP) | (1<<RESET) | (1<<MS3) | (1<<MS2) | (1<<MS1) | (1<<MOT_EN);
    DDRB |= (1<<DIRECTION) | (1<<STEP);
  
    // Domyslne stany portow
    PORTD |= (1<<SLEEP) | (1<<RESET);
  
    // Timer
  
    TCCR1A = (1<<COM1A1)|(1<<COM1B1)|(1<<WGM10)|(1<<WGM11);
    TCCR1B = (1<<CS11)|(1<<WGM12);
    OCR1A = 512;
  
    TIMSK |= 1<<TOIE1;
    TCNT0 = 128;
    
    UART_Init(MYUBRR);
    
    UART_Send("UART test ok\r\n");
    
    sei();
    
    while(1) {
	if(command_ready == TRUE) {
	    if(UART_data_in[0] == 'q') PORTB ^= (1<<DIRECTION);
	    if(UART_data_in[0] == 'w') PORTD ^= (1<<SLEEP);
	    if(UART_data_in[0] == 'e') PORTD ^= (1<<RESET);
	    if(UART_data_in[0] == 'r') PORTD ^= (1<<MS3);
	    if(UART_data_in[0] == 't') PORTD ^= (1<<MS2);
	    if(UART_data_in[0] == 'y') PORTD ^= (1<<MS1);
	    if(UART_data_in[0] == 'u') PORTD ^= (1<<MOT_EN);
	    command_ready = FALSE;
	}
    };

}

ISR (TIMER1_OVF_vect) {
  if(cnt == 4000) {
    //PORTD ^=(1<<MOT_EN);
    //PORTD ^=(1<<SLEEP);
    //PORTD ^=(1<<RESET);
    cnt = 1;
  }
  cnt++;
}

ISR (USART_RXC_vect) {
    UART_data_in[UART_data_count] = UDR;
    //if (UART_data_in[UART_data_count] == ';') {
	command_ready = TRUE;
	UART_data_count = 0;
    //}
    //else UART_data_count++;
}
