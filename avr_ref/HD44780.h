#ifndef _HD44780_H
#define _HD44780_H

#define PortLCD_Command PORTC
#define PortLCD_Command_DDR DDRC

#define PortDATA PORTD
#define PortDATA_DDR DDRD

#define LCD_En 2
#define LCD_RW 1
#define LCD_RS 0

extern void Enable(void);
extern void InitLCD(void);
extern void send_char(char chr);
extern void send_string(const char* str, int str_len);
extern void lcd_clear(void);
extern void place_cursor(unsigned char line, unsigned char position);

#endif
