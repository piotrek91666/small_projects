#include <avr/io.h>
#include <util/delay.h>

#include "HD44780.h"

void Enable(void) {
  PortLCD_Command |= (1 << LCD_En); // _BV(LCD_En)	
  _delay_us(2);
  PortLCD_Command &= ~(1 << LCD_En);
}

void InitLCD(void) {
  PortDATA_DDR = 0xFF;
  PortLCD_Command_DDR = 0xFF;
  
  PortLCD_Command &= ~((1 << LCD_RW) | (1 << LCD_RS));
  
  PortDATA = 0x30;
  Enable();
  _delay_ms(10);
  
  PortDATA = 0x30;
  Enable();
  _delay_ms(1);
  
  PortDATA = 0x30;
  Enable();
  _delay_ms(1);
  
  PortDATA = 0x38;
  Enable();
  _delay_ms(10);
  
  PortDATA = 0x08;
  Enable();
  _delay_us(100);
  
  PortDATA = 0x01;
  Enable();
  _delay_ms(10);
  
  PortDATA = 0x06;
  Enable();
  _delay_us(100);
  
  PortDATA = 0x0F;
  Enable();
  _delay_us(100);
}

void send_char(char chr) {
  PortLCD_Command |= (1 << LCD_RS);
  PortDATA = chr;
  Enable();
  _delay_us(100);
}

void lcd_clear() {
  PortLCD_Command &= ~((1 << LCD_RW) | (1 << LCD_RS));
  PortDATA = 0x01;
  Enable();
  _delay_ms(10);
}

void send_string(const char* str, int str_len) {
  for (int i = 0; i<str_len; i++) {
    send_char(str[i]);
  }
}

void place_cursor (unsigned char line, unsigned char position) {
  PortLCD_Command &= ~(1 << LCD_RS);
  if(line == 2) PortDATA = (position - 1) + 0x80 + 0x40;
  if(line == 1) PortDATA = (position - 1) + 0x80;
  Enable();
  _delay_us(100);
  
}
