#include <avr/io.h>
#include <stdio.h>
#include <string.h>
#include <util/delay.h>

#include "HD44780.h"

#define KL_G 1
#define KL_D 2

int main(void) {
  
  char init[] = "INIT";
  char start[] = "START";
  char stop[] = "STOP";
  char licz_str[8];
  
  int t_stop = 1000;
  
  DDRB = 0x00;
  PORTB = 0;
  
  InitLCD();
  
  while(1) {
    int counter = 0;
    lcd_clear();
    place_cursor(1,1);
    send_string(init, strlen(init));
    
    while(PINB & (1 << KL_G));
    
    lcd_clear();
    place_cursor(1,1);
    send_string(start, strlen(start));
    
    _delay_ms(t_stop);
    
    place_cursor(7,1);
    send_string(stop, strlen(stop));
    
    while(PINB & (1 << KL_D)) {
      _delay_ms(1);
      counter++;	
    }
    
    place_cursor(1,2);
    sprintf(licz_str, "%d", counter);
    send_string(licz_str, strlen(licz_str));
    send_string("ms", 2);

  }
}

