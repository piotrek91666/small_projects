#include <avr/io.h>
#include <util/delay.h>

#define LED_PIN (1<<PC1)
#define LED_ON PORTC &= ~LED_PIN
#define LED_OFF PORTC |= LED_PIN


int main(void) {
  DDRC |= LED_PIN;
  unsigned int time = 500;
  while(1){
    
    LED_ON;
    _delay_ms(time);
    LED_OFF;
    _delay_ms(time);
    
  }
}
