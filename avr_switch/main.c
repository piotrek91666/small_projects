#include <avr/io.h>
#include <util/delay.h>

#define LED_PIN (1<<5)
#define LED_TOG PORTC ^= LED_PIN

#define KEY_PIN (1<<4)
#define KEY_DOWN !(PINC & KEY_PIN)

uint8_t key_pressed(uint8_t key) {
  if(!(PINC & key)){
    _delay_ms(80);
    if(!(PINC & key)){
      return 1;
    }
  }
  return 0;
}

int main(void) {
  DDRC |= LED_PIN;
  DDRC &= ~KEY_PIN;
  PORTC |= LED_PIN;
  PORTC |= KEY_PIN;
  
  while(1){
    if(key_pressed(KEY_PIN)) LED_TOG;
  }
}
