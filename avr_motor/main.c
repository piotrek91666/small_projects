#include <avr/io.h>
#include <util/delay.h>

int main(void) {
  
  DDRD = 0xFF;
  DDRB = 0xFF;
  PORTD = 0b00000000;
  PORTB = 0b00000011;

  unsigned int time = 1;
  unsigned int pause_time = 2000;
  unsigned int l_count = 0;
  unsigned int angle = 90;
  
  unsigned int l = 0;
  while(1){
    for(l=0;l<9;l++){
      for(l_count = 1; l_count <=10; l_count++){
	
	PORTD = 0b00001100;
	_delay_ms(time);
	
	PORTD = 0b00000100;
	_delay_ms(time);
	
	PORTD = 0b00000110;
	_delay_ms(time);
	
	PORTD = 0b00000010;
	_delay_ms(time);
	
	PORTD = 0b00000011;
	_delay_ms(time);
	
	PORTD = 0b00000001;
	_delay_ms(time);
	
	PORTD = 0b00001001;
	_delay_ms(time);
	
	PORTD = 0b00001000;
	_delay_ms(time);
      }
      _delay_ms(500);
    }
    
    _delay_ms(pause_time);
    
    for(l_count = 1; l_count <=angle; l_count++){
      
      PORTD = 0b00001000;
      _delay_ms(time);
      
      PORTD = 0b00001001;
      _delay_ms(time);
      
      PORTD = 0b00000001;
      _delay_ms(time);
      
      PORTD = 0b00000011;
      _delay_ms(time);
      
      PORTD = 0b00000010;
      _delay_ms(time);
      
      PORTD = 0b00000110;
      _delay_ms(time);
      
      PORTD = 0b00000100;
      _delay_ms(time);
      
      PORTD = 0b00001100;
      _delay_ms(time);
      
    }
    
    _delay_ms(pause_time);
  }
}
