#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#define SRV_PORT 79

// Funkcja pobierajaca z bazy, pliku, czegokolwiek co zostanie w tym
// miejscu zaimplementowane dane o uzytkowniku
void fgrGetData(char *user, char *data) {
  char flName[128], uShell[128], uDir[128];
  int errHandler = 0;
  
  if(strcmp(user, "piotrek") == 0) {
    strcpy(flName, "Piotr G");
    strcpy(uShell, "/bin/bash");
    strcpy(uDir, "/home/piotrek");
  }
  else if(strcmp(user, "stefan") == 0) {
    strcpy(flName, "Stefan Wapniak");
    strcpy(uShell, "/bin/csh");
    strcpy(uDir, "/home/stefano");
  }
  else {
    errHandler = 1;
  }
  
  // formatowanie tekstu i danych
  if(!errHandler) {
    strcpy(data, "Login: ");
    strcat(data, user);
    strcat(data, "\n");
    
    strcat(data, "Name: ");
    strcat(data, flName);
    strcat(data, "\n");
    
    strcat(data, "Directory: ");
    strcat(data, uDir);
    strcat(data, "\n");
    
    strcat(data, "Shell: ");
    strcat(data, uShell);
    strcat(data, "\n");
  }
  else
    strcpy(data, "Wystapil blad, nie ma takiego uzytkownika.\n");
}

void fixUName(char *user){
  short i;
  for (i = 0; i < strlen(user); i++) {
    if (user[i] == '\0' || user[i] == '\r' || user[i] == '\n') {
      user[i] = '\0';
      break;
    }
  }
}

int main(){
  
  int sock, conn, bytes_recv, sin_size, truea = 1;
  char fgrUserName[32], data_send[1024];
  
  struct sockaddr_in srv_addr, cli_addr;
  
  if((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
    perror("Socket error.");
    exit(1);
  }
  
  if(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, & truea, sizeof(int)) == -1) {
    perror("Setsockopt error.");
    exit(1);
  }
  
  srv_addr.sin_family = AF_INET;
  srv_addr.sin_port = htons(SRV_PORT);
  srv_addr.sin_addr.s_addr = INADDR_ANY;
  
  bzero(&(srv_addr.sin_zero), 8);
  
  if(bind(sock,(struct sockaddr*) &srv_addr, sizeof(struct sockaddr)) == -1) {
    perror("Bind error");
    exit(1);
  }
  
  if(listen(sock, 5) == -1) {
    perror("Listen error");
    exit(1);
  }
  
  printf("Serwer finger oczekuje na klienta na porcie %d\n", SRV_PORT);
  
  fflush(stdout);
  
  while(1) {
    // obsluga polaczenia tcp
    sin_size = sizeof(struct sockaddr_in);
    conn = accept(sock,(struct sockaddr*) &cli_addr, &sin_size );
    printf("Polaczon z %s\n", inet_ntoa(cli_addr.sin_addr));
        
    // pobiera dane z fingera, czyli nazwe uzytkownika
    bytes_recv = recv(conn, fgrUserName, 1024, 0 );
    fgrUserName[bytes_recv] = '\0';
    fixUName(fgrUserName);
    printf("Query user: %s\n", fgrUserName );
    
    // stosownie do nazwy uzytkownika tworzy dane do wyslania i wysyla
    fgrGetData(fgrUserName, data_send);
    send(conn, data_send, strlen(data_send), 0);
    
    // zamyka polaczenie
    close(conn);
  }
  
  close(sock);
  return 0;
}
