#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdarg.h>
#include <resolv.h>

#include <netdb.h>

#define CLI_PORT 80

#define RCVBUFSIZE 64
#define ISspace(x) isspace((int)(x))

// --------------------------------------------------------------------------------------------------------------

int main(int argc, char *argv[]){
  int httpc = 0, sock_true = 1, conn, bytes_recv;
  
  char data_recv[1024];
  
  struct sockaddr_in cli_addr;
  struct hostent *hostaddr;
  char *request;
  
  
  if ( argc != 2 ) {
    printf("Args error \n");
    exit(1);
  }
  
  // Pobieramy adres
  printf("Resolving host: %s ...\n", argv[1]);
  if (!(hostaddr = gethostbyname(argv[1]))) {
    printf("Error resolving host: %s\n", argv[1]);
    exit(1);
  }
  else {
    printf("...ok -> %s\n", hostaddr->h_name);
  }
    
  
  // TCP
  
  
  bzero(&cli_addr, sizeof(cli_addr));
  
  
  cli_addr.sin_family = AF_INET;
  cli_addr.sin_port = htons(CLI_PORT);
  memcpy(&cli_addr.sin_addr, hostaddr->h_addr, hostaddr->h_length);
  
  httpc = socket(AF_INET, SOCK_STREAM, 0);
  if (httpc == -1) {
    perror("socket");
    exit(1);
  }
  
  conn = connect(httpc, (struct sockaddr*) &cli_addr, sizeof(cli_addr));
  if(conn < 0) {
    printf("Connection error: %d\n", conn);
  }
  
  printf("Connected: %s:%d\n", inet_ntoa(cli_addr.sin_addr), ntohs(cli_addr.sin_port));
  
  request = "GET / HTTP/1.1\r\n\r\n";
  
  printf("================================================\nSending request:\n%s\n", request);
  
  if(send(httpc , request , strlen(request), 0) < 0){
    puts("Send failed");
    return 1;
  }
  
  if(recv(httpc, data_recv , 1024 , 0) < 0)
    {
      printf("recv failed");
    }
    printf("================================================\n");
    printf("%s", data_recv);
  
    

  
  printf("\n");
  
  close(httpc);
  
  return 0;
}
